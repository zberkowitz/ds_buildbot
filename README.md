# Dockerized BuildBot for `ds_ros` CI

Steps to set this beast up from scratch on another machine.

- Install docker
- Install docker-compose
- (make sure your username is added to the docker group)
- build the ros docker image:
  - `cd ds_ros/docker/ros-kinetic`
  - `docker build . -t ds_ros/ros-buildbot-worker:kinetic`
- Add files to `ds_ros/secrets`:
  - `ds_ros/secrets/bitbucket_user`: contains the username to log into bitbucket with
  - `ds_ros/secrets/bitbucket_password`: the password for the above user name
- set permissions on these files 400 (read access only for user)
- Run docker compose:
  - `cd ds_ros && docker-compose up`


